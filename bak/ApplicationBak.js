/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */



Ext.define('DatalogClient.Application', {
    extend: 'Ext.app.Application',
    
    name: 'DatalogClient',
    
    requires:[
        'DatalogClient.DirectAPI'
    ],

  	views: [
        'main.Main',
        'TreeActions'
    ],
    stores: [
        // TODO: add global / shared stores here
        'Tree'
    ],
    
	launch: function(){
		
		Ext.require('DatalogClient.view.main.Main');
		
        if(DatalogClient.DirectError){
            Ext.Msg.alert('Error', Demo.DirectError.message);
        } else {
            //Note that we have removed autoCreateViewport property from app.js and instantiate it here.
            //This allows us to block application execution if Direct backend is not available to serve the requests.
            //var viewport = Ext.create('DatalogClient.view.main.Main');

            //Let's check if we are logged in

            Server.Auth.Login.checkLogin({},
                function(result, event) {
                	console.log('ok');
                	/*
                    var tabs = viewport.down('tabpanel').items.items;

                    if(result.auth) {
                        // enable other tabs
                        Ext.each(tabs, function(cmp){
                            cmp.enable();
                        });
                    } */
                }
            );
        }
    }
});
