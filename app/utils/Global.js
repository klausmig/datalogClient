Ext.define('DatalogClient.utils.global', {
    singleton: true,
    nodes: null,
    edges: null,
    msnodes: null,
    msedges: null,
    db_id: null,
    MSgenerated: false,
    selectedRulesId: null,
    loadDb: function(db_id, toReload) {
        //console.log('texting');

        Server.Demo.Interpreter.loadDb({
                db_id: db_id,
                toReload: toReload
            },
            function(result, event) {

                //nsole.log(JSON.stringify(nodesSelected));
                if (result.success) {
                    if (result.isJustLoaded) {

                        DatalogClient.utils.global.nodes = result.nodes;
                        DatalogClient.utils.global.edges = result.edges;

                        //console.log(JSON.stringify(result.nodes));

                        Ext.getCmp('dgTab').load("cy/index.html?nodes=" + Ext.encode(result.nodes) + "&edges=" + Ext.encode(result.edges));

                    }

                    DatalogClient.utils.global.db_id = db_id;
                } else {

                    Ext.getCmp('logPanel').expand(true);
                    Ext.getCmp('dgTab').load("cy/index.html?nodes=" + Ext.encode([]) + "&edges=" + Ext.encode([]));

                }

                Ext.getStore('Fact').load({
                    params: {
                        db_id: db_id
                    }
                });
                Ext.getStore('RuleStore').load({
                    params: {
                        db_id: db_id
                    }
                });

                Ext.getStore('MSStore').loadData([], false);
                Ext.getStore('SipsStore').loadData([], false);
                Ext.getStore('QueryStore').loadData([], false);

                Ext.getStore('ConsoleStore').load();
            }
        );
    }

});
