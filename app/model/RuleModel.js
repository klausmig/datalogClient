Ext.define('DatalogClient.model.RuleModel', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id'
        //,mapping: '_id'  // IMPORTANT! Uncomment for MongoDB backend example
    }, {
        name: 'num'
        //,mapping: '_id'  // IMPORTANT! Uncomment for MongoDB backend example
    }, {
        name: 'id_db'

    }, {
        name: 'value'
    }],

    proxy: {
        type: 'direct',
        api: {
            create: 'Server.Demo.Rule.create',
            read: 'Server.Demo.Rule.read',
            update: 'Server.Demo.Rule.update',
            destroy: 'Server.Demo.Rule.destroy'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message' // mandatory if you want the framework to set message property content
        }
    }
});
