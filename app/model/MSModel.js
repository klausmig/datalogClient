Ext.define('DatalogClient.model.MSModel', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'id'
            //,mapping: '_id'  // IMPORTANT! Uncomment for MongoDB backend example
        },
        {
            name: 'rule'
        }
    ],

    proxy: {
        type: 'direct',
        api: {
            //create:  'Server.Demo.Rule.create'
            read: 'Server.Demo.Interpreter.getMagicRules'
            // update:  'Server.Demo.Rule.update',
            // destroy: 'Server.Demo.Rule.destroy'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message' // mandatory if you want the framework to set message property content
        }
    }
});
