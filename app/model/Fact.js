Ext.define('DatalogClient.model.Fact', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'id'
            //,mapping: '_id'  // IMPORTANT! Uncomment for MongoDB backend example
        },
        {
            name: 'id_db'

        },
        {
            name: 'value'
        }
    ],

    proxy: {
        type: 'direct',
        api: {
            create: 'Server.Demo.Fact.create',
            read: 'Server.Demo.Fact.read',
            update: 'Server.Demo.Fact.update',
            destroy: 'Server.Demo.Fact.destroy'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            messageProperty: 'message' // mandatory if you want the framework to set message property content
        }
    }
});
