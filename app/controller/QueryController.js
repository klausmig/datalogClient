Ext.define('DatalogClient.controller.QueryController', {
    extend: 'Ext.app.Controller',

    refs: [{
        ref: 'editor',
        selector: 'query-actions #todoForm'
    }, {
        ref: 'todoGrid',
        selector: 'query-actions #todoGrid'
    }],

    init: function(application) {
        this.control({
            /*
                      'grid-actions #todoGrid': {
                          itemclick: this.onTodoGridItemClick
                      },

                      'grid-actions button': { //listening for all buttons on grid-actions here, then narrow down to particular button inside actual method
                          click: this.buttonActions
                      },

                      'grid-actions #todoGrid toolbar #filter': {
                          reset: function() { //not the best practice, please avoid if possible! this only shows that you can use dashes for event names.
                              //we can define logic also here
                              Ext.getStore('Fact').clearFilter();
                          }
                      }, */
            'query-actions #todoGrid': {
                itemclick: this.onTodoGridItemClick
            },

            'query-actions button': { //listening for all buttons on grid-actions here, then narrow down to particular button inside actual method
                click: this.buttonActions
            },

            'query-actions #todoGrid toolbar #filter': {
                reset: function() { //not the best practice, please avoid if possible! this only shows that you can use dashes for event names.
                    //we can define logic also here
                    Ext.getStore('QueryStore').clearFilter();
                }
            }
        });
    },

    //improves excessive query overhead
    buttonActions: function(button, e, eOpts) {
        var me = this;

        switch (button.action) {
            case 'insertRecord':
                me.onInsertBtnClick();
                break;
            case 'updateRecord':
                me.onUpdateBtnClick();
                break;
            case 'removeRecord':
                me.onRemoveBtnClick();
                break;
            case 'queryDb':
                me.queryDb();
                break;
            case 'filterStore':
                me.filterStore();
                break;
            default:
                break;
        }
    },

    queryDb: function() {

        var nodesSelected = Ext.getCmp('treePanel').getSelectionModel().getLastSelected();

        if (nodesSelected) {

            //console.log('nodesSelected:'+nodesSelected);
            var field = this.getTodoGrid().down('toolbar #queryInput'),
                value = field.getValue(),
                store = Ext.getStore('QueryStore');
            store.load({
                params: {
                    db_id: nodesSelected.data.db_id,
                    query_string: value
                },
                callback: function(records, operation, success) {

                    // if (success == true) {
                    //     if (records.length == 0) {
                    //         Ext.Msg.alert('Resultado', 'No se ha encontrado informaci&oacute;n');
                    //     }
                    // }
                    if (success == false) {

                        // Ext.getCmp('logTable').scrollTo(1, 100, true);
                        // Ext.getCmp('logPanel').scrollTo(1, 100, true);

                        Ext.getCmp('logPanel').expand(true);

                        //console.log("scroll");

                        // try {
                        //     Ext.Msg.alert('Error', operation.getError()); // way more elegant than ussing rawData etc ...
                        // } catch (e) {
                        //     Ext.Msg.alert('Error', 'Error  inesperado en el servidor.');
                        // }
                    }
                }
            });

            store.on('load', function(store, records, options) {
                // console.log('succesfully loaded');

                Ext.getStore('ConsoleStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id
                    }
                });
            })

        }
    },

    filterStore: function() {
        var field = this.getTodoGrid().down('toolbar #filter'),
            value = field.getValue(),
            store = Ext.getStore('QueryStore');

        if (value) {
            store.clearFilter(true);
            store.filter('text', value); // filter on 'text' field
        }
    },

    onTodoGridItemClick: function(dataview, record, item, index, e, eOpts) {
        var form = this.getEditor();
        /* form.getForm().loadRecord(record);
         form.enable(); */
    },

    onInsertBtnClick: function() {
        var store = Ext.getStore('QueryStore');
        var record = Ext.create('Demo.model.Fact', {
            text: 'New todo action ' + +(store.getCount() + 1),
            complete: 0
        });
        record.save({
            callback: function(records, operation, success) {
                //we add to store only after successful insertion at the server-side
                if (success) {
                    Ext.getStore('QueryStore').add(records);
                } else {
                    query.log('Failure to add record: ', arguments);
                }
            }
        });
    },

    onRemoveBtnClick: function() {
        var me = this;
        if (this.missingSelection()) {
            Ext.Msg.alert('Error', 'Please select record to remove');
        } else {
            var form = me.getEditor().getForm(),
                record = form.getRecord(),
                store = Ext.getStore('QueryStore');

            me.getTodoGrid().getSelectionModel().deselect(record);

            store.remove(record);

            record.erase({
                callback: function(records, operation) {
                    var success = operation.wasSuccessful();
                    form.reset();
                    me.getEditor().disable();
                    if (success) {
                        query.log('Sucessfully removed record: ', arguments);
                    } else {
                        store.insert(record.index, record);
                        query.log('Failure to remove record: ', arguments);
                        Ext.Msg.alert('Server side Error', 'Unable to remove the record');
                    }
                }
            });
        }
    },

    onUpdateBtnClick: function() {
        //prevent errors if no records selected
        if (this.missingSelection()) {
            return false;
        }

        var form = this.getEditor().getForm();

        if (form.isValid()) {
            var record = form.getRecord();
            form.updateRecord(record);

            record.save({
                success: function(record, operation) {
                    record.commit(); // ##Juris :: Commit record in the store
                    query.log('success', record, operation);
                    // update form from computed remote record
                    form.loadRecord(record);
                },
                failure: function(record, operation) {
                    var exception = operation.getError();
                    if (exception && exception.errors) form.markInvalid(exception.errors);
                    query.log('failure', record, operation, exception);
                },
                scope: this
            });
        }
    },

    missingSelection: function() {
        return this.getTodoGrid().getSelectionModel().getSelection().length === 0;
    }
});
