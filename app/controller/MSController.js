Ext.define('DatalogClient.controller.MSController', {
    extend: 'Ext.app.Controller',

    refs: [{
        ref: 'editor',
        selector: 'magic-rules #todoForm'
    }, {
        ref: 'todoGrid',
        selector: 'magic-rules #todoGrid'
    }],

    init: function(application) {
        this.control({
            /*
            'grid-actions #todoGrid': {
                itemclick: this.onTodoGridItemClick
            },

            'grid-actions button': { //listening for all buttons on grid-actions here, then narrow down to particular button inside actual method
                click: this.buttonActions
            },

            'grid-actions #todoGrid toolbar #filter': {
                reset: function() { //not the best practice, please avoid if possible! this only shows that you can use dashes for event names.
                    //we can define logic also here
                    Ext.getStore('Fact').clearFilter();
                }
            }, */
            'magic-rules #todoGrid': {
                itemclick: this.onTodoGridItemClick
            },

            'magic-rules button': { //listening for all buttons on grid-actions here, then narrow down to particular button inside actual method
                click: this.buttonActions
            },

            'magic-rules #todoGrid toolbar #filter': {
                reset: function() { //not the best practice, please avoid if possible! this only shows that you can use dashes for event names.
                    //we can define logic also here
                    Ext.getStore('MSStore').clearFilter();
                }
            }
        });
    },

    //improves excessive query overhead
    buttonActions: function(button, e, eOpts) {
        var me = this;

        switch (button.action) {
            case 'insertRecord':
                me.onInsertBtnClick();
                break;
            case 'updateRecord':
                me.onUpdateBtnClick();
                break;
            case 'removeRecord':
                me.onRemoveBtnClick();
                break;
            case 'generateMagicSets':
                me.generateMagicSetsBtnClick();
                break;
            case 'filterStore':
                me.filterStore();
                break;
            default:
                break;
        }
    },

    generateMagicSetsBtnClick: function() {
        //this.getTodoGrid().getStore().reload();
        console.log('click on boton generateMagicSetsBtnClick');
        var nodesSelected = Ext.getCmp('treePanel').getSelectionModel().getLastSelected();

        if (nodesSelected) {
            console.log('generateMagicSets Event:' + nodesSelected.data.db_id);

            var store = Ext.getStore('MSStore');
            var magicRulesTab = Ext.getCmp('dgMSTab');
            var tabPanel = Ext.getCmp('tabPanel');

            tabPanel.setActiveTab(magicRulesTab);
            store.load({
                params: {
                    db_id: nodesSelected.data.db_id,
                    selectedRowsId: JSON.stringify(DatalogClient.utils.global.selectedRowsId),
                    generate: true
                }
            });
            magicRulesTab.show();

            store.on('load', function(store, records, options) {
                //console.log('succesfully loaded');
                Ext.getStore('SipsStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id,
                        selectedRowsId: JSON.stringify(DatalogClient.utils.global.selectedRowsId)
                    }
                });
                Ext.getStore('ConsoleStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id
                    }
                });

                Server.Demo.Interpreter.getDirectedGraphMS({
                        db_id: '1'
                    },
                    function(result, event) {

                        //nsole.log(JSON.stringify(nodesSelected));

                        DatalogClient.utils.global.msnodes = result.nodes;
                        DatalogClient.utils.global.msedges = result.edges;

                        DatalogClient.utils.global.MSgenerated = true;

                        //console.log(JSON.stringify(result.nodes));

                        Ext.getCmp('dgMSTab').load("cy/index.html?nodes=" + Ext.encode(result.nodes) + "&edges=" + Ext.encode(result.edges));

                    }
                );
            })

        }
    },

    filterStore: function() {
        var field = this.getTodoGrid().down('toolbar #filter'),
            value = field.getValue(),
            store = Ext.getStore('MSStore');

        if (value) {
            store.clearFilter(true);
            store.filter('text', value); // filter on 'text' field
        }
    },

    onTodoGridItemClick: function(dataview, record, item, index, e, eOpts) {
        /*        var form = this.getEditor();
                form.getForm().loadRecord(record);
                form.enable();*/
    },

    onInsertBtnClick: function() {
        var store = Ext.getStore('MSStore');
        var record = Ext.create('Demo.model.Fact', {
            text: 'New todo action ' + +(store.getCount() + 1),
            complete: 0
        });
        record.save({
            callback: function(records, operation, success) {
                //we add to store only after successful insertion at the server-side
                if (success) {
                    Ext.getStore('MSStore').add(records);
                } else {
                    console.log('Failure to add record: ', arguments);
                }
            }
        });
    },

    onRemoveBtnClick: function() {
        var me = this;
        if (this.missingSelection()) {
            Ext.Msg.alert('Error', 'Please select record to remove');
        } else {
            var form = me.getEditor().getForm(),
                record = form.getRecord(),
                store = Ext.getStore('MSStore');

            me.getTodoGrid().getSelectionModel().deselect(record);

            store.remove(record);

            record.erase({
                callback: function(records, operation) {
                    var success = operation.wasSuccessful();
                    form.reset();
                    me.getEditor().disable();
                    if (success) {
                        console.log('Sucessfully removed record: ', arguments);
                    } else {
                        store.insert(record.index, record);
                        console.log('Failure to remove record: ', arguments);
                        Ext.Msg.alert('Server side Error', 'Unable to remove the record');
                    }
                }
            });
        }
    },

    onUpdateBtnClick: function() {
        //prevent errors if no records selected
        if (this.missingSelection()) {
            return false;
        }

        var form = this.getEditor().getForm();

        if (form.isValid()) {
            var record = form.getRecord();
            form.updateRecord(record);

            record.save({
                success: function(record, operation) {
                    record.commit(); // ##Juris :: Commit record in the store
                    console.log('success', record, operation);
                    // update form from computed remote record
                    form.loadRecord(record);
                },
                failure: function(record, operation) {
                    var exception = operation.getError();
                    if (exception && exception.errors) form.markInvalid(exception.errors);
                    console.log('failure', record, operation, exception);
                },
                scope: this
            });
        }
    },

    missingSelection: function() {
        return this.getTodoGrid().getSelectionModel().getSelection().length === 0;
    }
});
