Ext.define('DatalogClient.view.RuleActions', {
    extend: 'Ext.container.Container',

    xtype: 'rule-actions',

    title: 'Rule',

    layout: 'border',

    style: 'padding:5px',

    id: 'rulePanel',

    height: 200,

    items: [{
        xtype: 'gridpanel',
        region: 'center',
        itemId: 'todoGrid',
        store: 'RuleStore',
        selModel: new Ext.selection.RowModel({
            mode: "MULTI"
        }),

        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                    xtype: 'button',
                    action: 'insertRecord',
                    icon: 'resources/assets/plus-circle.png',
                    text: 'Insert Rule'
                }, {
                    text: 'Reload Rules',
                    icon: 'resources/assets/arrow-circle-double-135.png',
                    action: 'loadStore'
                },
                '->', {
                    xtype: 'textfield',
                    itemId: 'filter',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: 1, // controls display order
                            hideOnReadOnly: false, //always visible
                            handler: function(trigger) {
                                trigger.reset();
                                this.fireEvent('reset', trigger);
                            }
                        }
                    },
                    emptyText: 'Filter'
                }, {
                    xtype: 'button',
                    action: 'filterStore',
                    text: 'Filter'
                }
            ]
        }, {
            xtype: 'pagingtoolbar',
            store: 'RuleStore', // same store GridPanel is using
            dock: 'bottom',
            plugins: new Ext.ux.ProgressBarPager(),
            displayInfo: true
        }],

        columns: [{
                dataIndex: 'id',
                text: 'Id',
                width: 70,
                hidden: true
            }, {
                flex: 1,
                dataIndex: 'value',
                text: 'Rules'
            }
            /*,
                            {
                                dataIndex: 'Parsed',
                                renderer: function(value){return value ? 'Done' : 'Not yet'},
                                text: 'Parsed'
                            },{
                                xtype: 'actioncolumn',
                                width: 20,
                                items: [{
                                    icon: 'resources/assets/information.png',
                                    tooltip: 'Click for more info',
                                    handler: function(grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        Ext.Msg.alert('Item description', 'Task: ' + rec.get('text'));
                                    }
                                }]
                            }*/
        ],
        listeners: {
            itemclick: function(dv, record, item, index, e) {
                var selectedRec = dv.getSelectionModel().getSelected();
                //console.log(selectedRec.get('value')); //Will display text of name column of selected record

                //Ext.getCmp('fact-actions #todoForm').expand(true);

                Ext.getCmp('editRule').expand(true);
            },
            select: function(selModel, record, index, options) {
                //console.log(index);
            },
            selectionchange: function(sModel, sRecords) {

                for (var i = 0; i < sRecords.length; i++)
                    console.log(sRecords[i].get('id'));

            }
        }

    }, {
        xtype: 'form',
        id: 'editRule',
        region: 'east',
        split: true,
        disabled: true,
        width: 350,
        itemId: 'todoForm',
        collapsible: true,
        collapsed: true,
        bodyPadding: 10,
        title: 'Edit Rule',
        iconCls: 'glyphicon glyphicon-pencil',
        //icon: 'resources/assets/pencil.png',
        defaults: {
            anchor: '100%'
        },
        items: [{
            xtype: 'textfield',
            disabled: true,
            fieldLabel: 'Id',
            name: 'id'
        }, {
            xtype: 'textfield',
            allowBlank: false,
            msgTarget: 'side',
            fieldLabel: 'Value',
            name: 'value'
        }, {
            xtype: 'checkboxfield',
            fieldLabel: 'Complete',
            name: 'complete',
            boxLabel: 'Yes'
        }],

        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                    xtype: 'button',
                    action: 'removeRecord',
                    icon: 'resources/assets/minus-circle.png',
                    text: 'Remove selected'
                },
                '->', {
                    xtype: 'button',
                    action: 'updateRecord',
                    icon: 'resources/assets/pencil.png',
                    text: 'Update'
                }
            ]
        }]
    }]
});
