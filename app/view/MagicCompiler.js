Ext.define('DatalogClient.view.MagicCompiler', {
    extend: 'Ext.TabPanel',

    requires: [

        'Ext.ux.ProgressBarPager'
    ],

    title: 'Sample TextArea',
    xtype: 'magic-compiler',
    width: 400,
    //bodyPadding: 10,

    //style: 'padding:3px 3px',

    title: 'Magic Compiler',
    //glyph: 'xf085@FontAwesome',

    iconCls: 'fa fa-star-half-o',

    region: 'center',

    //style: 'padding:5px',
    items: [{
        xtype: 'uxiframe',
        title: "MS Dependency Graph",
        id: 'dgMSTab',
        //xtype:'leafletmapview',
        src: "cy/index.html",
        //iconCls: 'fa fa-picture-o',
        iconCls: 'fa fa-area-chart'
        // glyph: 'xf1c5@FontAwesome'

    }, {
        title: 'Magic Sets',
        xtype: 'magic-rules',

        iconCls: 'fa fa-magic'
        //glyph: 'xf0d0@FontAwesome'

    }, {
        title: 'Sips Rules',
        xtype: 'sips-rules',
        id: 'sipsRulesTab',
        iconCls: 'glyphicon glyphicon-sort-by-attributes'
        //glyph: 'xe155@Glyphicons Halflings'
    }]

});
