/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('DatalogClient.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'
    ],

    alias: 'controller.main',

    loadDb: function(bt) {

        var nodesSelected = Ext.getCmp('treePanel').getSelectionModel().getLastSelected();

        if (nodesSelected) {
            //console.log('nodesSelected.data.db_id' + nodesSelected.data.db_id);
            DatalogClient.utils.global.loadDb(nodesSelected.data.db_id, true);

            // var store = Ext.getStore('MSStore');
            // var magicRulesTab = Ext.getCmp('magicRulesTab');
            // var tabPanel = Ext.getCmp('tabPanel');

            // tabPanel.setActiveTab(magicRulesTab);
            // store.load({
            //     params: {
            //         db_id: nodesSelected.data.db_id
            //     }
            // });
            // magicRulesTab.show();

            // store.on('load', function(store, records, options) {
            //     console.log('succesfully loaded');
            //     Ext.getStore('SipsStore').load({
            //         params: {
            //             db_id: nodesSelected.data.db_id
            //         }
            //     });
            //     Ext.getStore('ConsoleStore').load({
            //         params: {
            //             db_id: nodesSelected.data.db_id
            //         }
            //     });
            // })

            //Server.Demo.Interpreter.getMagicRules({db_id:nodesSelected.data.db_id},
            //    function (result, event) {

            //        //nsole.log(JSON.stringify(nodesSelected));

            //        var magicRulesTab = Ext.getCmp('magicRulesTab');
            //        var tabPanel = Ext.getCmp('tabPanel');
            //        tabPanel.setActiveTab(magicRulesTab);

            //        // you can grab useful info from event
            //        var transaction = event.getTransaction(),
            //            status = event.status;
            //        console.log(status);

            //        //Ext.getCmp('magicTextArea').setValue(Ext.encode(result));
            //        //Ext.getCmp('magicTextArea').setValue(result.msg);

            //    }
            //);
        }

    },

    onClickButton: function(bt) {

        var nodesSelected = Ext.getCmp('treePanel').getSelectionModel().getLastSelected();

        if (nodesSelected) {
            console.log('nodesSelected.data.db_id' + nodesSelected.data.db_id);

            var store = Ext.getStore('MSStore');
            var magicRulesTab = Ext.getCmp('magicRulesTab');
            var tabPanel = Ext.getCmp('tabPanel');

            tabPanel.setActiveTab(magicRulesTab);
            store.load({
                params: {
                    db_id: nodesSelected.data.db_id
                }
            });
            magicRulesTab.show();

            store.on('load', function(store, records, options) {
                console.log('succesfully loaded');
                Ext.getStore('SipsStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id
                    }
                });
                Ext.getStore('ConsoleStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id
                    }
                });
            })

            //Server.Demo.Interpreter.getMagicRules({db_id:nodesSelected.data.db_id},
            //    function (result, event) {

            //        //nsole.log(JSON.stringify(nodesSelected));

            //        var magicRulesTab = Ext.getCmp('magicRulesTab');
            //        var tabPanel = Ext.getCmp('tabPanel');
            //        tabPanel.setActiveTab(magicRulesTab);

            //        // you can grab useful info from event
            //        var transaction = event.getTransaction(),
            //            status = event.status;
            //        console.log(status);

            //        //Ext.getCmp('magicTextArea').setValue(Ext.encode(result));
            //        //Ext.getCmp('magicTextArea').setValue(result.msg);

            //    }
            //);
        }

    },

    onClickDG: function() {

        Server.Demo.Interpreter.getDirectedGraph({
            db_id: '1'
        }, function(result, event) {

            //nsole.log(JSON.stringify(nodesSelected));

            DatalogClient.utils.global.nodes = result.nodes;
            DatalogClient.utils.global.edges = result.edges;

            console.log(JSON.stringify(result.nodes));

            Ext.getCmp('dgTab').load("cy/index.html?nodes=" + Ext.encode(result.nodes) + "&edges=" + Ext.encode(result.edges));

            //var magicRulesTab = Ext.getCmp('magicRulesTab');
            //var tabPanel = Ext.getCmp('tabPanel');
            //tabPanel.setActiveTab(magicRulesTab);

            //// you can grab useful info from event
            //var transaction = event.getTransaction(),
            //    status = event.status;
            //console.log(status);

            //Ext.getCmp('magicTextArea').setValue(Ext.encode(result));
            //Ext.getCmp('magicTextArea').setValue(result.msg);

        });
        //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    reloadDependencyGraph: function() {

        if (DatalogClient.utils.global.db_id != null) {
            DatalogClient.utils.global.loadDb(DatalogClient.utils.global.db_id, true);

            Server.Demo.Interpreter.getDirectedGraph({
                db_id: '1'
            }, function(result, event) {

                //nsole.log(JSON.stringify(nodesSelected));

                DatalogClient.utils.global.nodes = result.nodes;
                DatalogClient.utils.global.edges = result.edges;

                //console.log(JSON.stringify(result.nodes));

                Ext.getCmp('dgTab').load("cy/index.html?nodes=" + Ext.encode(result.nodes) + "&edges=" + Ext.encode(result.edges));

                Ext.getCmp('dgTab').show();

            });
        }
    },

    onConfirm: function(choice) {
        if (choice === 'yes') {
            //
        }
    },

    generateMagicSets: function() {
        //this.getTodoGrid().getStore().reload();
        console.log('click on boton generateMagicSetsBtnClick');
        var nodesSelected = Ext.getCmp('treePanel').getSelectionModel().getLastSelected();

        if (nodesSelected) {
            console.log('generateMagicSets Event:' + nodesSelected.data.db_id);

            var store = Ext.getStore('MSStore');
            var magicRulesTab = Ext.getCmp('dgMSTab');
            var tabPanel = Ext.getCmp('tabPanel');

            tabPanel.setActiveTab(magicRulesTab);
            store.load({
                params: {
                    db_id: nodesSelected.data.db_id,
                    selectedRowsId: JSON.stringify(DatalogClient.utils.global.selectedRowsId),
                    generate: true
                }
            });
            magicRulesTab.show();

            store.on('load', function(store, records, options) {
                //console.log('succesfully loaded');
                Ext.getStore('SipsStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id,
                        selectedRowsId: JSON.stringify(DatalogClient.utils.global.selectedRowsId)
                    }
                });
                Ext.getStore('ConsoleStore').load({
                    params: {
                        db_id: nodesSelected.data.db_id
                    }
                });

                Server.Demo.Interpreter.getDirectedGraphMS({
                        db_id: '1'
                    },
                    function(result, event) {

                        //nsole.log(JSON.stringify(nodesSelected));

                        DatalogClient.utils.global.msnodes = result.nodes;
                        DatalogClient.utils.global.msedges = result.edges;

                        DatalogClient.utils.global.MSgenerated = true;

                        //console.log(JSON.stringify(result.nodes));

                        Ext.getCmp('dgMSTab').load("cy/index.html?nodes=" + Ext.encode(result.nodes) + "&edges=" + Ext.encode(result.edges));

                    }
                );
            })

        }
    }
});
