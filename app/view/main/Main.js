/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('DatalogClient.view.main.Main', {
    extend: 'Ext.container.Viewport',
    requires: [
        'DatalogClient.view.main.MainController',
        'DatalogClient.view.main.MainModel'
    ],

    xtype: 'app-main',

    controller: 'main',

    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [{
            region: 'north',
            xtype: 'component',
            cls: 'appBanner',
            //iconCls: 'fa fa-home',
            padding: 10,
            height: 40,

            html: '<i class="fa fa-home"></i>  Datalog Manager<small> v0.1.0</small>'
        }, {
            xtype: 'panel',
            // bind: {
            // 	title: '{name}'
            // },
            title: 'Datalog Databases',
            //iconCls: 'glyphicon glyphicon-list',
            iconCls: 'fa fa-th-list',
            region: 'west',
            //html: '<ul><li>This area is commonly used for navigation, for example, using a "tree" component.</li></ul>',
            width: 250,
            split: true,
            collapsible: true,

            // tbar: [{
            //         xtype: 'button',
            //         //text: 'Load DB',
            //         icon: 'resources/assets/arrow-circle-double-135.png',
            //         //action: 'loadDb'
            //         handler: 'loadDb'
            //     }, {
            //         xtype: 'button',
            //         //action: 'insertRecord',
            //         icon: 'resources/assets/plus-circle.png',
            //         //text: 'Insert blank record'
            //         disabled: true
            //     }, {
            //         xtype: 'button',
            //         // action: 'removeRecord',
            //         icon: 'resources/assets/minus-circle.png',
            //         // text: 'Remove selected'
            //         disabled: true
            //     }, {
            //         xtype: 'button',
            //         // action: 'updateRecord',
            //         icon: 'resources/assets/pencil.png',
            //         // text: 'Update'
            //         disabled: true
            //     }, {
            //         text: 'Magic Rules',
            //         handler: 'onClickButton'
            //     }, {
            //         text: 'DirectGraph',
            //         handler: 'onClickDG'
            //     }

            // ],

            items: [{
                    xtype: 'tree-actions',
                    disabled: false
                }

            ]
        }, {
            region: 'center',
            xtype: 'tabpanel',
            id: 'tabPanel',
            items: [{
                    //title: 'Magic Compiler',

                    xtype: 'database-tab',

                    id: 'databaseTab',
                    layout: {
                        type: 'border'
                    },

                    dockedItems: [{
                        xtype: 'toolbar',
                        dock: 'top',
                        items: [{
                                text: 'Reload Dependency Graph',
                                icon: 'resources/assets/arrow-circle-double-135.png',
                                //iconCls: 'fa fa-magic',
                                //glyph: 'xf0d0@FontAwesome',
                                //action: 'generateMagicSets'
                                handler: 'reloadDependencyGraph'
                            },
                            //{
                            //    xtype: 'button',
                            //    action: 'insertRecord',
                            //    icon: 'resources/assets/plus-circle.png',
                            //    text: 'Insert blank record'
                            //},

                        ]
                    }],
                }, {
                    title: 'Magic Compiler',
                    //glyph: 'xf085@FontAwesome',
                    xtype: 'magic-compiler',

                    id: 'magicRulesTab',
                    layout: {
                        type: 'border'
                    },

                    dockedItems: [{
                        xtype: 'toolbar',
                        dock: 'top',
                        items: [{
                                text: 'Generate Magic Sets',
                                //icon: 'resources/assets/run.png',
                                //iconCls: 'fa fa-magic',
                                glyph: 'xf0d0@FontAwesome',
                                //action: 'generateMagicSets'
                                handler: 'generateMagicSets'
                            },
                            //{
                            //    xtype: 'button',
                            //    action: 'insertRecord',
                            //    icon: 'resources/assets/plus-circle.png',
                            //    text: 'Insert blank record'
                            //},

                        ]
                    }],

                    //iconCls: 'glyphicon glyphicon-indent-left'
                    //glyph: 'xe057@Glyphicons Halflings',

                    // tbar: [{
                    //     text: 'Generate Magic Set',
                    //     // icon: 'resources/assets/arrow-circle-double-135.png',
                    //     //iconCls: 'fa fa-magic',
                    //     glyph: 'xf0d0@FontAwesome',
                    //     action: 'generateMagicSets',
                    //     xtype: 'button',
                    //     //text: 'Load DB',
                    //     //icon: 'resources/assets/arrow-circle-double-135.png',
                    //     //action: 'loadDb'
                    //     handler: 'loadDb'

                    //     }, ],

                }, {
                    title: 'Query Evaluator',
                    xtype: 'query-actions',
                    id: 'queryTab',
                    //iconCls: 'glyphicon glyphicon-search'
                    iconCls: 'fa fa-search'
                    //glyph: 'xe003@Glyphicons Halflings'

                }


            ]
        }, {
            region: 'south',
            xtype: 'console-actions',
            title: 'Console',
            split: true,
            collapsed: true,
            collapsible: true,
            id: 'logPanel',
            height: 340,
            iconCls: 'fa fa-info'

        }


    ]
});
