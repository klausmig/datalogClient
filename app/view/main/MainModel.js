/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('DatalogClient.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'DatalogClient'
    }

    //TODO - add data, formulas and/or methods to support your view
});
