Ext.define('DatalogClient.view.DatabaseTab', {
    extend: 'Ext.TabPanel',

    requires: [

    'Ext.ux.ProgressBarPager'
  ],

    title: 'Sample TextArea',
    xtype: 'database-tab',
    width: 400,
    //bodyPadding: 10,

    //style: 'padding:3px 3px',

    title: 'Datalog Database',
    //glyph: 'xf085@FontAwesome',

    iconCls: 'fa fa-database',

    region: 'center',

    //style: 'padding:5px',
    items: [{
        xtype: 'uxiframe',
        title: "Dependency Graph",
        id: "dgTab",
        //xtype:'leafletmapview',
        src: "cy/index.html",
        iconCls: 'fa fa-area-chart'
        //glyph: 'xf1c5@FontAwesome'

  }, {
        title: 'Rules',
        xtype: 'rule-actions',
        id: 'ruleTab',
        iconCls: 'glyphicon glyphicon-indent-left'
        //glyph: 'xe057@Glyphicons Halflings'
  }, {
        /*  title: 'Fact',
          xtype: 'grid',
          store: {
              fields:['name', 'email', 'phone'],
              data:[
                  { name: 'Lisa',  email: "lisa@simpsons.com",
                    phone: "555-111-1224"  },
                  { name: 'Bart',  email: "bart@simpsons.com",
                    phone: "555-222-1234" },
                  { name: 'Homer', email: "home@simpsons.com",
                    phone: "555-222-1244"  },
                  { name: 'Marge', email: "marge@simpsons.com",
                    phone: "555-222-1254"  }
              ],
              proxy: {
                  type: 'memory'
              }
          },
          columns: [
              { text: 'Name',  dataIndex: 'name' },
              { text: 'Email', dataIndex: 'email', flex: 1},
              { text: 'Phone', dataIndex: 'phone' }
          ]*/

        title: 'Facts',
        xtype: 'fact-actions',
        id: 'factTab',
        //iconCls: 'glyphicon glyphicon-list',
        //glyph: 'xf0ce@FontAwesome'
        iconCls: 'fa fa-table'

        //glyph: 'xe012@Glyphicons Halflings'

        //&#xe012;

  }]

});
