Ext.define('DatalogClient.view.ConsoleActions', {
    extend: 'Ext.form.FormPanel',

    requires: [
        'Ext.ux.ProgressBarPager'
    ],

    title: 'Sample TextArea',
    xtype: 'console-actions',
    width: 400,
    //bodyPadding: 10,
    layout: 'border',

    style: 'padding:5px',

    items: [{
            xtype: 'gridpanel',
            region: 'center',
            itemId: 'todoGrid',
            id: 'logTable',
            //id:'sipsTextArea',
            store: 'ConsoleStore',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    /*
                {
                    xtype: 'textfield',
                    itemId: 'queryInput',
                    emptyText: 'Query'
                },
                {
                    text: 'Ask',
                    icon: 'resources/assets/arrow-circle-double-135.png',
                    action: 'loadStore'
                }, */
                    '->', {
                        xtype: 'textfield',
                        itemId: 'filter',
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: 1, // controls display order
                                hideOnReadOnly: false, //always visible
                                handler: function(trigger) {
                                    trigger.reset();
                                    this.fireEvent('reset', trigger);
                                }
                            }
                        },
                        emptyText: 'Filter'
                    }, {
                        xtype: 'button',
                        action: 'filterStore',
                        text: 'Filter'
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'ConsoleStore', // same store GridPanel is using
                dock: 'bottom',
                plugins: new Ext.ux.ProgressBarPager(),
                displayInfo: true
            }],

            columns: [{
                    dataIndex: 'id',
                    text: 'Id',
                    width: 70
                }, {
                    dataIndex: 'log',
                    text: 'log',
                    flex: 1

                }
                /* {
                dataIndex: 'Parsed',
                renderer: function(value){return value ? 'Done' : 'Not yet'},
                text: 'Parsed'
            },{
                xtype: 'actioncolumn',
                width: 20,
                items: [{
                    icon: 'resources/assets/information.png',
                    tooltip: 'Click for more info',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        Ext.Msg.alert('Item description', 'Task: ' + rec.get('text'));
                    }
                }

				]
            } */
            ]
        }

        //{
        //    xtype     : 'textareafield',
        //    grow      : true,
        //    name      : 'Console',
        //    fieldLabel: 'Magic Rules',
        //    anchor    : '100%',
        //	height:'40%',
        //	id:'magicTextArea'
        //},

        //{
        //    xtype: 'textareafield',
        //    region: 'south',
        //    grow      : true,
        //    name      : 'console',
        //    fieldLabel: 'console',
        //    anchor    : '100%',
        //	height:'40%'
        //}
    ]
});
