Ext.define('DatalogClient.view.QueryActions', {
    extend: 'Ext.form.FormPanel',

    requires: [

        'Ext.ux.ProgressBarPager'
		],

    title: 'Sample TextArea',
    xtype: 'query-actions',
    width: 400,
    //bodyPadding: 10,
    layout: 'border',

    style: 'padding:5px',
    id: 'queryPanel',

    items: [{
            xtype: 'gridpanel',
            region: 'center',
            itemId: 'todoGrid',
            //id:'sipsTextArea',
            store: 'QueryStore',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'textfield',
                        itemId: 'queryInput',
                        value: 'q(X)'
                },
                    {
                        text: 'Ask',
                        // icon: 'resources/assets/arrow-circle-double-135.png',
                        action: 'queryDb',
                        glyph: 'xf128@FontAwesome'
                },
                '->',
                    {
                        xtype: 'textfield',
                        itemId: 'filter',
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: 1, // controls display order
                                hideOnReadOnly: false, //always visible
                                handler: function(trigger) {
                                    trigger.reset();
                                    this.fireEvent('reset', trigger);
                                }
                            }
                        },
                        emptyText: 'Filter'
                }, {
                        xtype: 'button',
                        action: 'filterStore',
                        text: 'Filter'
                }
            ]
        }, {
                xtype: 'pagingtoolbar',
                store: 'QueryStore', // same store GridPanel is using
                dock: 'bottom',
                plugins: new Ext.ux.ProgressBarPager(),
                displayInfo: true
        }],

            columns: [
                {
                    dataIndex: 'id',
                    text: 'Id',
                    width: 70
            },
                {
                    dataIndex: 'result',
                    text: 'Result',
                    flex: 1

            }
           /* {
                dataIndex: 'Parsed',
                renderer: function(value){return value ? 'Done' : 'Not yet'},
                text: 'Parsed'
            },{
                xtype: 'actioncolumn',
                width: 20,
                items: [{
                    icon: 'resources/assets/information.png',
                    tooltip: 'Click for more info',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        Ext.Msg.alert('Item description', 'Task: ' + rec.get('text'));
                    }
                }

				]
            } */
        ]
    }

    //{
    //    xtype     : 'textareafield',
    //    grow      : true,
    //    name      : 'Query',
    //    fieldLabel: 'Magic Rules',
    //    anchor    : '100%',
	//	height:'40%',
	//	id:'magicTextArea'
    //},

    //{
    //    xtype: 'textareafield',
    //    region: 'south',
    //    grow      : true,
    //    name      : 'Query',
    //    fieldLabel: 'Query',
    //    anchor    : '100%',
	//	height:'40%'
    //}
    ]
});
