Ext.define('DatalogClient.view.TreeActions', {
    extend: 'Ext.tree.Panel',

    store: 'Tree',

    id: 'treePanel',

    border: false,

    xtype: 'tree-actions',

    // title: ' ',
    //iconCls: 'fa fa-database',

    rootVisible: false,
    // tbar: [{
    // 		text: 'Magic Rules',
    // 		handler: 'onClickButton'
    // 	}, {
    // 		text: 'DirectGraph',
    // 		handler: 'onClickDG'
    // 	}

    // ],

    listeners: {

        itemclick: function(s, r) {

            if (DatalogClient.utils.global.db_id != r.data.db_id) {

                DatalogClient.utils.global.selectedRowsId = null;

                DatalogClient.utils.global.loadDb(r.data.db_id, false);

                //console.log(JSON.stringify(r.data));
                //console.log('DatalogClient.utils.global.db_id:' + DatalogClient.utils.global.db_id + "  r.data.db_id:" + r.data.db_id);

                var dgTab = Ext.getCmp('databaseTab');
                var tabPanel = Ext.getCmp('tabPanel');
                tabPanel.setActiveTab(dgTab);

                dgTab.show();

                if (DatalogClient.utils.global.MSgenerated) {
                    Ext.getCmp('dgMSTab').load("cy/index.html?nodes=" + Ext.encode([]) + "&edges=" + Ext.encode([]));
                    DatalogClient.utils.global.MSgenerated = false;
                }
            }

            var pref = r.data.id.substring(0, 3);

            if (r.data.leaf) {

                if (pref == "fa_") {

                    var store = Ext.getStore('Fact');

                    var factTab = Ext.getCmp('factTab');
                    var tabPanel = Ext.getCmp('tabPanel');
                    tabPanel.setActiveTab(factTab);
                    store.load({
                        params: {
                            relation: r.data.text,
                            db_id: r.data.db_id
                        }
                    });
                    factTab.show();
                } else {

                    var store = Ext.getStore('RuleStore');

                    var ruleTab = Ext.getCmp('ruleTab');
                    var tabPanel = Ext.getCmp('tabPanel');
                    tabPanel.setActiveTab(ruleTab);
                    store.load({
                        params: {
                            relation: r.data.text,
                            db_id: r.data.db_id
                        }
                    });
                    ruleTab.show();

                }

            } else {

                if (pref == "fa_") {

                    var factTab = Ext.getCmp('factTab');
                    var tabPanel = Ext.getCmp('tabPanel');
                    Ext.getStore('Fact').load({
                        params: {
                            db_id: r.data.db_id
                        }
                    });
                    tabPanel.setActiveTab(factTab);
                    factTab.show();

                }

                if (pref == "ru_") {

                    var ruleTab = Ext.getCmp('ruleTab');
                    var tabPanel = Ext.getCmp('tabPanel');
                    Ext.getStore('RuleStore').load({
                        params: {
                            db_id: r.data.db_id
                        }
                    });

                    tabPanel.setActiveTab(ruleTab);
                    ruleTab.show();

                }

            }

        }
    }
});
