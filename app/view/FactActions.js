Ext.define('DatalogClient.view.FactActions', {
    extend: 'Ext.container.Container',
    xtype: 'fact-actions',
    title: 'Fact',
    layout: 'border',
    style: 'padding:5px',
    id: 'factPanel',
    height: 200,

    items: [
        {
            xtype: 'gridpanel',
            region: 'center',
            itemId: 'todoGrid',
            store: 'Fact',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        action: 'insertRecord',
                        icon: 'resources/assets/plus-circle.png',
                        //iconCls: 'glyphicon glyphicon-plus',
                        //iconCls: 'fa fa-plus-circle ',
                        //glyph: 'xf055@FontAwesome',
                        text: 'Insert a Fact'
                    },
                    {
                        text: 'Reload Facts',
                        icon: 'resources/assets/arrow-circle-double-135.png',
                        //iconCls: 'glyphicon glyphicon-refresh',
                        //iconCls: 'fa fa-refresh',
                        action: 'loadStore'
                    },
                    '->',
                    {
                        xtype: 'textfield',
                        itemId: 'filter',
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: 1, // controls display order
                                hideOnReadOnly: false, //always visible
                                handler: function(trigger) {
                                    trigger.reset();
                                    this.fireEvent('reset', trigger);
                                }
                            }
                        },
                        emptyText: 'Filter'
                    }, {
                        xtype: 'button',
                        action: 'filterStore',
                        text: 'Filter'
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'Fact', // same store GridPanel is using
                dock: 'bottom',
                plugins: new Ext.ux.ProgressBarPager(),
                displayInfo: true

            }],

            columns: [
                {
                    dataIndex: 'id',
                    text: 'Id',
                    width: 70

                },
                {
                    flex: 1,
                    dataIndex: 'value',
                    text: 'Fact'
                }
                /*,
                                {
                                    dataIndex: 'Parsed',
                                    renderer: function(value){return value ? 'Done' : 'Not yet'},
                                    text: 'Parsed'
                                },{
                                    xtype: 'actioncolumn',
                                    width: 20,
                                    items: [{
                                        icon: 'resources/assets/information.png',
                                        tooltip: 'Click for more info',
                                        handler: function(grid, rowIndex, colIndex) {
                                            var rec = grid.getStore().getAt(rowIndex);
                                            Ext.Msg.alert('Item description', 'Task: ' + rec.get('value'));
                                        }
                                    }]
                                }*/
            ],
            listeners: {
                itemclick: function(dv, record, item, index, e) {
                    var selectedRec = dv.getSelectionModel().getSelected();
                    //console.log(selectedRec.get('value')); //Will display text of name column of selected record

                    //Ext.getCmp('fact-actions #todoForm').expand(true);

                    Ext.getCmp('editFact').expand(true);
                }
            }

        },
        {
            xtype: 'form',
            id: 'editFact',
            region: 'east',
            split: true,
            disabled: true,
            width: 350,
            itemId: 'todoForm',
            collapsible: true,
            collapsed: true,
            bodyPadding: 10,
            title: 'Edit Fact',
            iconCls: 'glyphicon glyphicon-pencil',
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'textfield',
                    disabled: true,
                    fieldLabel: 'Id',
                    name: 'id'
                },
                {
                    xtype: 'textfield',
                    allowBlank: false,
                    msgTarget: 'side',
                    fieldLabel: 'Value',
                    name: 'value'
                },
                {
                    xtype: 'checkboxfield',
                    fieldLabel: 'Complete',
                    name: 'complete',
                    boxLabel: 'Yes'
                }
            ],

            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            action: 'removeRecord',
                            //icon: 'resources/assets/minus-circle.png',
                            iconCls: 'glyphicon glyphicon-remove',
                            //iconCls: ' fa fa-times',

                            //cls:'btn btn-success',
                            text: 'Remove selected'
                        },
                        '->',
                        {
                            xtype: 'button',
                            action: 'updateRecord',
                            //icon: 'resources/assets/pencil.png',
                            iconCls: 'glyphicon glyphicon-pencil',
                            text: 'Update'
                        }
                    ]
                }
            ]
        }
    ]
});
