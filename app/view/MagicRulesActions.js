Ext.define('DatalogClient.view.MagicRulesActions', {
    extend: 'Ext.form.FormPanel',

    requires: [

        'Ext.ux.ProgressBarPager'
    ],

    xtype: 'magic-rules',
    width: 400,
    //bodyPadding: 10,
    layout: 'border',
    //style: 'padding:2px',
    id: 'magicPanel',

    items: [{
            xtype: 'panel',
            region: 'center',
            split: true,
            height: '100%',
            width: '100%',
            //collapsible: true,
            //collapsed: false,
            title: 'Magic Rules',
            //iconCls: 'glyphicon glyphicon-pencil',
            defaults: {
                anchor: '100%'
            },
            layout: 'border',

            items: [{
                xtype: 'gridpanel',
                itemId: 'todoGrid',
                region: 'center',
                style: 'padding:3px',
                store: 'MSStore',
                dockedItems: [{
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                    // {
                    //         text: 'Generate Dependency Graph',
                    //         // icon: 'resources/assets/arrow-circle-double-135.png',
                    //         //iconCls: 'fa fa-magic',
                    //         glyph: 'xf0d0@FontAwesome',
                    //         action: 'generateMagicSets'
                    //     },
                        //{
                        //    xtype: 'button',
                        //    action: 'insertRecord',
                        //    icon: 'resources/assets/plus-circle.png',
                        //    text: 'Insert blank record'
                        //},

                        '->', {
                            xtype: 'textfield',
                            itemId: 'filter',
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    weight: 1, // controls display order
                                    hideOnReadOnly: false, //always visible
                                    handler: function(trigger) {
                                        trigger.reset();
                                        this.fireEvent('reset', trigger);
                                    }
                                }
                            },
                            emptyText: 'Filter'
                        }, {
                            xtype: 'button',
                            action: 'filterStore',
                            text: 'Filter'
                        }
                    ]
                }, {
                    xtype: 'pagingtoolbar',
                    store: 'MSStore', // same store GridPanel is using
                    dock: 'bottom',
                    plugins: new Ext.ux.ProgressBarPager(),
                    displayInfo: true
                }],

                columns: [{
                        dataIndex: 'id',
                        text: 'Id',
                        width: 70,
                        hidden: true
                }, {
                        flex: 1,
                        dataIndex: 'rule',
                        text: 'Rule'
                }
                    /*, {
                                        dataIndex: 'Parsed',
                                        renderer: function(value) {
                                            return value ? 'Done' : 'Not yet'
                                        },
                                        text: 'Parsed'
                                    }, {
                                        xtype: 'actioncolumn',
                                        width: 20,
                                        items: [{
                                            icon: 'resources/assets/information.png',
                                            tooltip: 'Click for more info',
                                            handler: function(grid, rowIndex, colIndex) {
                                                var rec = grid.getStore().getAt(rowIndex);
                                                Ext.Msg.alert('Item description', 'Task: ' + rec.get('text'));
                                            }
                                        }]
                                        }
                                    */
                    ]
            }]
        }, {
            xtype: 'panel',
            region: 'west',
            split: true,
            height: '100%',
            width: 350,
            itemId: 'todoForm',
            collapsible: true,
            collapsed: false,
            title: 'DB Rules',
            //iconCls: 'glyphicon glyphicon-pencil',
            defaults: {
                anchor: '100%'
            },
            layout: 'border',
            items: [{
                title: 'DB Rules',
                xtype: 'rule-actions-ms',
                region: 'center',
                id: 'ruleTabMS',
                //iconCls: 'glyphicon glyphicon-indent-left'
                glyph: 'xe057@Glyphicons Halflings'
            }]
        }
        /*{
            xtype: 'form',
            region: 'east',
            split: true,
            disabled: true,
            width: 350,
            itemId: 'todoForm',
            collapsible: true,
            collapsed: true,
            bodyPadding: 10,
            title: 'Edit Magic Rule',
            iconCls: 'glyphicon glyphicon-pencil',
            defaults: {
                anchor: '100%'
            },
            items: [{
                xtype: 'textfield',
                disabled: true,
                fieldLabel: 'Id',
                name: 'id'
            }, {
                xtype: 'textfield',
                allowBlank: false,
                msgTarget: 'side',
                fieldLabel: 'Rule',
                name: 'rule'
            }, {
                xtype: 'checkboxfield',
                fieldLabel: 'Complete',
                name: 'complete',
                boxLabel: 'Yes'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                        xtype: 'button',
                        action: 'removeRecord',
                        icon: 'resources/assets/minus-circle.png',
                        text: 'Remove selected'
                    },
                    '->', {
                        xtype: 'button',
                        action: 'updateRecord',
                        icon: 'resources/assets/pencil.png',
                        text: 'Update'
                    }
                ]
            }]
        }*/

        //{
        //    xtype     : 'textareafield',
        //    grow      : true,
        //    name      : 'magicrules',
        //    fieldLabel: 'Magic Rules',
        //    anchor    : '100%',
        //	height:'40%',
        //	id:'magicTextArea'
        //},

        //{
        //    xtype: 'textareafield',
        //    region: 'south',
        //    grow      : true,
        //    name      : 'sips',
        //    fieldLabel: 'SIPS',
        //    anchor    : '100%',
        //	height:'40%'
        //}
    ]
});
