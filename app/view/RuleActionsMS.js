Ext.define('DatalogClient.view.RuleActionsMS', {
    extend: 'Ext.container.Container',

    xtype: 'rule-actions-ms',

    title: 'DB Rules',

    layout: 'border',

    id: 'rulePanelMS',

    height: 200,

    items: [{
        xtype: 'gridpanel',
        region: 'center',
        itemId: 'todoGrid',
        store: 'RuleStore',
        selModel: new Ext.selection.RowModel({
            mode: "MULTI"
        }),
        style: 'padding:3px',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [

                '->', {
                    xtype: 'textfield',
                    itemId: 'filter',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: 1, // controls display order
                            hideOnReadOnly: false, //always visible
                            handler: function(trigger) {
                                trigger.reset();
                                this.fireEvent('reset', trigger);
                            }
                        }
                    },
                    emptyText: 'Filter'
                }, {
                    xtype: 'button',
                    action: 'filterStore',
                    text: 'Filter'
                }
            ]
        }, {
            xtype: 'pagingtoolbar',
            store: 'RuleStore', // same store GridPanel is using
            dock: 'bottom',
            plugins: new Ext.ux.ProgressBarPager(),
            displayInfo: true
        }],

        columns: [{
                dataIndex: 'id',
                text: 'Id',
                width: 70,
                hidden: true
            }, {
                dataIndex: 'num',
                text: 'num',
                width: 70
            }, {
                flex: 1,
                dataIndex: 'value',
                text: 'Original rule'
            }
            /*         {
                         dataIndex: 'Parsed',
                         renderer: function(value){return value ? 'Done' : 'Not yet'},
                         text: 'Parsed'
                     },{
                         xtype: 'actioncolumn',
                         width: 20,
                         items: [{
                             icon: 'resources/assets/information.png',
                             tooltip: 'Click for more info',
                             handler: function(grid, rowIndex, colIndex) {
                                 var rec = grid.getStore().getAt(rowIndex);
                                 Ext.Msg.alert('Item description', 'Task: ' + rec.get('text'));
                             }
                         }]
                     }*/
        ],
        listeners: {
            itemclick: function(dv, record, item, index, e) {
                var selectedRec = dv.getSelectionModel().getSelected();
                //console.log(selectedRec.get('value')); //Will display text of name column of selected record

                //Ext.getCmp('fact-actions #todoForm').expand(true);

                Ext.getCmp('editRule').expand(true);
            },
            selectionchange: function(sModel, sRecords) {

                var selectedIds = new Array();
                console.log("select event");

                for (var i = 0; i < sRecords.length; i++) {
                    console.log(sRecords[i].get('num'));
                    selectedIds.push(sRecords[i].get('num'));
                }

                console.log(selectedIds);
                DatalogClient.utils.global.selectedRowsId = selectedIds;
                console.log(DatalogClient.utils.global.selectedRowsId);

                var store = Ext.getStore('MSStore');

                store.load({
                    params: {
                        selectedRowsId: JSON.stringify(DatalogClient.utils.global.selectedRowsId),
                        generate: false
                    }
                });

                store.on('load', function(store, records, options) {
                    //console.log('succesfully loaded');
                    Ext.getStore('SipsStore').load({
                        params: {
                            selectedRowsId: JSON.stringify(DatalogClient.utils.global.selectedRowsId)
                        }
                    });
                });

            }
        }

    }]
});
