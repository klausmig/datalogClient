Ext.define('DatalogClient.view.SipsRulesActions', {
    extend: 'Ext.form.FormPanel',

    requires: [

        'Ext.ux.ProgressBarPager'
    ],

    title: 'Sample TextArea',
    xtype: 'sips-rules',
    width: 400,
    //bodyPadding: 10,
    layout: 'border',

    style: 'padding:5px',
    id: 'sipsPanel',

    items: [{
            xtype: 'gridpanel',
            region: 'center',
            itemId: 'todoGrid',
            id: 'sipsTextArea',
            store: 'SipsStore',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    // {
                    //     xtype: 'button',
                    //     action: 'insertRecord',
                    //     icon: 'resources/assets/plus-circle.png',
                    //     text: 'Insert blank record'
                    // }, {
                    //     text: 'Load data',
                    //     icon: 'resources/assets/arrow-circle-double-135.png',
                    //     action: 'loadStore'
                    // },
                    '->', {
                        xtype: 'textfield',
                        itemId: 'filter',
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: 1, // controls display order
                                hideOnReadOnly: false, //always visible
                                handler: function(trigger) {
                                    trigger.reset();
                                    this.fireEvent('reset', trigger);
                                }
                            }
                        },
                        emptyText: 'Filter'
                    }, {
                        xtype: 'button',
                        action: 'filterStore',
                        text: 'Filter'
                    }
                ]
            }, {
                xtype: 'pagingtoolbar',
                store: 'SipsStore', // same store GridPanel is using
                dock: 'bottom',
                plugins: new Ext.ux.ProgressBarPager(),
                displayInfo: true
            }],

            columns: [{
                    dataIndex: 'id',
                    text: 'Id',
                    width: 70,
                    hidden: true
                }, {

                    dataIndex: 'rule',
                    text: 'Original rule',
                    width: 320
                }, {
                    dataIndex: 'adorment',
                    text: 'Adornment',
                    width: 120
                }, {
                    flex: 1,
                    dataIndex: 'sip-rule',
                    text: 'Orded rule'
                }
                /* {
                dataIndex: 'Parsed',
                renderer: function(value){return value ? 'Done' : 'Not yet'},
                text: 'Parsed'
            },{
                xtype: 'actioncolumn',
                width: 20,
                items: [{
                    icon: 'resources/assets/information.png',
                    tooltip: 'Click for more info',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        Ext.Msg.alert('Item description', 'Task: ' + rec.get('text'));
                    }
                }

				]
            } */
            ]
        }, {
            xtype: 'form',
            region: 'east',
            split: true,
            disabled: true,
            width: 350,
            itemId: 'todoForm',
            collapsible: true,
            collapsed: true,
            bodyPadding: 10,
            title: 'Edit Sips Rule',
            iconCls: 'glyphicon glyphicon-pencil',
            defaults: {
                anchor: '100%'
            },
            items: [{
                xtype: 'textfield',
                disabled: true,
                fieldLabel: 'Id',
                name: 'id'
            }, {
                xtype: 'textfield',
                allowBlank: false,
                msgTarget: 'side',
                fieldLabel: 'Rule',
                name: 'rule'
            }, {
                xtype: 'checkboxfield',
                fieldLabel: 'Complete',
                name: 'complete',
                boxLabel: 'Yes'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                        xtype: 'button',
                        action: 'removeRecord',
                        icon: 'resources/assets/minus-circle.png',
                        text: 'Remove selected'
                    },
                    '->', {
                        xtype: 'button',
                        action: 'updateRecord',
                        icon: 'resources/assets/pencil.png',
                        text: 'Update'
                    }
                ]
            }]
        }

        //{
        //    xtype     : 'textareafield',
        //    grow      : true,
        //    name      : 'SipsRules',
        //    fieldLabel: 'Magic Rules',
        //    anchor    : '100%',
        //	height:'40%',
        //	id:'magicTextArea'
        //},

        //{
        //    xtype: 'textareafield',
        //    region: 'south',
        //    grow      : true,
        //    name      : 'sips',
        //    fieldLabel: 'SIPS',
        //    anchor    : '100%',
        //	height:'40%'
        //}
    ]
});
