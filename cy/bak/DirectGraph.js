var cy;

$(function() { // on dom ready

  cy = cytoscape({
    container: document.getElementById('cy'),

    boxSelectionEnabled: false,
    autounselectify: true,

    style: cytoscape.stylesheet()
      .selector('node')
      .css({
        'content': 'data(id)',
        'background-color': '#61bffc',
        'width': 25,
        'height': 25

      })
      .selector('edge')
      .css({
        'source-arrow-shape': 'triangle',
        'width': 4,
        'line-color': '#ddd',
        'source-arrow-color': '#ddd'
      })
      .selector('.important')
      .css({
        'background-color': '#79d279',
        'line-color': ' #79d279',
        // 'line-color': ' #ff4d4d',
        //'target-arrow-color': ' #ff8080',
        'source-arrow-color': ' #79d279',
        'transition-property': 'background-color, line-color, target-arrow-color',
        'transition-duration': '0.5s'
      }),
    .selector('.highlighted')
    .css({
      'background-color': '#61bffc',
      'line-color': '#61bffc',
      'source-arrow-color': '#61bffc',
      'transition-property': 'background-color, line-color, target-arrow-color',
      'transition-duration': '0.5s'
    }),

    elements: {
      nodes: [
        /*
              { data: { id: 'a' } },
               { data: { id: 'b' } },
               { data: { id: 'c' } },
               { data: { id: 'd' } },
               { data: { id: 'e' } }  */
      ],

      edges: [
        /*   { data: { id: 'a"e', weight: 1, source: 'a', target: 'e' } },
           { data: { id: 'ab', weight: 3, source: 'a', target: 'b' } },
           { data: { id: 'be', weight: 4, source: 'b', target: 'e' } },
           { data: { id: 'bc', weight: 5, source: 'b', target: 'c' } },
           { data: { id: 'ce', weight: 6, source: 'c', target: 'e' } },
           { data: { id: 'cd', weight: 2, source: 'c', target: 'd' } },
           { data: { id: 'de', weight: 7, source: 'd', target: 'e' } }*/
      ]
    },

    layout: {
      name: 'breadthfirst',
      directed: true,
      roots: '#p',
      padding: 10
    }
  });

  var bfs = cy.elements().bfs('#p', function() {}, true);

  var i = 0;
  var highlightNextEle = function() {
    if (i < bfs.path.length) {
      bfs.path[i].addClass('highlighted');
      i++;
      setTimeout(highlightNextEle, 1000);
    }
  };

  // kick off first highlight
  highlightNextEle();

}); // on dom ready