$(function() { // on dom ready

  function getSearchParameters() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
  }

  function transformToAssocArray(prmstr) {
    var params = {};
    var prmarr = prmstr.split("&");
    for (var i = 0; i < prmarr.length; i++) {
      var tmparr = prmarr[i].split("=");
      params[tmparr[0]] = tmparr[1];
    }
    return params;
  }

  var params = getSearchParameters();

  // if (params.nodes) {
  //   console.log(unescape(params.nodes));
  //   console.log(unescape(params.edges));
  // }

  cy = cytoscape({
    container: document.getElementById('cy'),

    boxSelectionEnabled: false,
    autounselectify: true,

    style: cytoscape.stylesheet()
      .selector('node')
      .css({
        'content': 'data(id)',
        'background-color': '#61bffc',
        'width': 25,
        'height': 25,
        'element-sizing': 'fixed',
        'text-sizing': 'fixed'
      })
      .selector('edge')
      .css({
        'source-arrow-shape': 'triangle',
        'width': 4,
        'line-color': '#ddd',
        'source-arrow-color': '#ddd',
        'label': 'data(label)'
      })
      // .selector('.highlighted')
      // .css({
      //   'background-color': '#61bffc',
      //   'line-color': '#61bffc',
      //   'source-arrow-color': '#61bffc',
      //   'transition-property': 'background-color, line-color, target-arrow-color',
      //   'transition-duration': '0.5s'
      // })
      .selector('.important')
      .css({
        'background-color': '#f0c2e0',
        'line-color': ' #f0c2e0',
        // 'line-color': ' #ff4d4d',
        //'target-arrow-color': ' #ff8080',
        'source-arrow-color': ' #f0c2e0',
        'transition-property': 'background-color, line-color, target-arrow-color',
        'transition-duration': '0.5s'
      })

      .selector('.l1')
      .css({
        'background-color': '#ff9933',
      })
      .selector('.l2')
      .css({
        'background-color': '#66cc99',
      })
      .selector('.l3')
      .css({
        'background-color': '#cc66cc',
      })
      .selector('.l4')
      .css({
        'background-color': '#8585ad',
      })
      .selector('.l5')
      .css({
        'background-color': ' #cc9966',
      })
      .selector('.l6')
      .css({
        'background-color': ' #bbbb77',
      })
      .selector('.critical')
      .css({
        'background-color': '#ffb3b3',
        'line-color': ' #ffb3b3',
        //'target-arrow-color': ' #ff8080',
        'source-arrow-color': ' #ffb3b3',
        'transition-property': 'background-color, line-color, target-arrow-color',
        'transition-duration': '0.5s'
      }),

    elements: {
      nodes: [
        /*
              { data: { id: 'a' } },
               { data: { id: 'b' } },
               { data: { id: 'c' } },
               { data: { id: 'd' } },
               { data: { id: 'e' } }  */
      ],

      edges: [
        /*   { data: { id: 'a"e', weight: 1, source: 'a', target: 'e' } },
           { data: { id: 'ab', weight: 3, source: 'a', target: 'b' } },
           { data: { id: 'be', weight: 4, source: 'b', target: 'e' } },
           { data: { id: 'bc', weight: 5, source: 'b', target: 'c' } },
           { data: { id: 'ce', weight: 6, source: 'c', target: 'e' } },
           { data: { id: 'cd', weight: 2, source: 'c', target: 'd' } },
           { data: { id: 'de', weight: 7, source: 'd', target: 'e' } }*/
      ]
    },

    zoom: 1,
    pan: {
      x: 0,
      y: 0
    },
    minZoom: 0.6,
    maxZoom: 1.5,
    zoomingEnabled: true,
    userZoomingEnabled: true,
    panningEnabled: true,
    userPanningEnabled: true,
    boxSelectionEnabled: false,
    selectionType: 'single',
    touchTapThreshold: 8,
    desktopTapThreshold: 4,
    autolock: false,
    autoungrabify: false,
    autounselectify: false,

    // rendering options:
    headless: false,
    styleEnabled: true,
    hideEdgesOnViewport: false,
    hideLabelsOnViewport: false,
    textureOnViewport: false,
    motionBlur: false,
    motionBlurOpacity: 0.2,
    wheelSensitivity: 0.2,
    pixelRatio: 'auto',

    layout: {
      name: 'breadthfirst',
      directed: true,
      roots: '#p',
      padding: 10
    }
  });

  //console.log(graphController.nodes);
  //var allNodes = parserRuleObj.nodes.concat(parserFactObj.nodes);
  //console.log(allNodes);

  if (params.nodes) {
    cy.add(JSON.parse(unescape(params.nodes)));
    cy.add(JSON.parse(unescape(params.edges)));
    //console.log(params.nodes);
    //console.log(params.edges);
  }
  //console.log(JSON.stringify(parserRuleObj.nodes));
  //console.log(JSON.stringify(parserRuleObj.edges));
  /*
  cy.layout({
    // name: 'breadthfirst',
    name: 'dagre',
    directed: true,
    padding: 10,
    roots: '#r',
  }); */

  cy.layout({
    // name: 'breadthfirst',
    name: 'dagre',
    directed: true,
    padding: 10,
    //roots: '#r',
  });

  var updateBounds = function() {
    var bounds = cy.elements().boundingBox();
    //console.log(bounds.w + 200);
    $('#cy').css('height', bounds.h + 100);
    $('#cy').css('width', bounds.w + 100);
    cy.center();
    cy.resize();
    //fix the Edgehandles
    //$('#cy').cytoscapeEdgehandles('resize');
  };

  cy.on('zoom', function() {
    cy.center();
  });

  cy.zoom(1);
  cy.center();

  updateBounds();

  cy.on('ready', function() {
    updateBounds();
  });
  //if they resize the window, resize the diagram
  $(window).resize(function() {
    updateBounds();
  });



}); // on dom ready