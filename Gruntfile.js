module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		jsbeautifier: {
			files: ['app/**/*.js','*.html','*.json'],
			options: {
				//config: "path/to/configFile",
				html: {
					fileTypes: [".html"],
					braceStyle: "collapse",
					indentChar: " ",
					indentScripts: "keep",
					indentSize: 4,
					maxPreserveNewlines: 1,
					preserveNewlines: false,
					unformatted: ["a", "sub", "sup", "b", "i", "u"],
					wrapLineLength: 0
				},
				css: {
					indentChar: " ",
					indentSize: 4
				},
				js: {
					fileTypes: [".js",".json"],
					braceStyle: "collapse",
					breakChainedMethods: false,
					e4x: false,
					evalCode: false,
					indentChar: " ",
					indentLevel: 0,
					indentSize: 4,
					indentWithTabs: false,
					jslintHappy: false,
					keepArrayIndentation: true,
					keepFunctionIndentation: false,
					maxPreserveNewlines: 2,
					preserveNewlines: true,
					spaceBeforeConditional: true,
					spaceInParen: false,
					unescapeStrings: false,
					wrapLineLength: 0,
					endWithNewline: true
				}
			}
		},

		babel: {
			options: {
				plugins: ['transform-es2015-modules-amd'
					/*, 
									          'transform-remove-console'*/
				],
				presets: ['es2015']
			},

			dist: {
				files: [{
					expand: true,
					cwd: 'src/main/webapp/js/scripts',
					src: ['**/*.js'],
					dest: 'src/main/webapp/js/tmp',
					ext: '.js'
				}]
			}
		},

		concat: {
			options: {
				banner: "(function(root) {\n",
				footer: "\n})(window);",
				separator: '\n'
			},
			dist: {
				options: {
					footer: "\nrequire('app.js')\n})(window);",
				},
				src: [
					'src/main/webapp/js/vendor/loader.js',
					'src/main/webapp/js/vendor/es6-promise.js',
					'src/main/webapp/js/vendor/animation-frame-shim.js',
					'src/main/webapp/js/vendor/modernizr-custom.js',
					'src/main/webapp/js/vendor/slideout.min.js',
					'src/main/webapp/js/tmp/<%= pkg.name %>.js'
				],
				dest: 'src/main/webapp/dist/<%= pkg.name %>.js'
			}
		},


		uglify: {
			dist: {
				src: 'src/main/webapp/dist/<%= pkg.name %>.js',
				dest: 'src/main/webapp/dist/<%= pkg.name %>.min.js'
			},
			options: {
				compress: {
					drop_console: true
				}
			},
		},

		requirejs: {
			compile: {
				options: {
					baseUrl: 'src/main/webapp/js/tmp',
					include: ['app.js'],
					out: 'src/main/webapp/js/tmp/<%= pkg.name %>.js'
				}
			}
		},

		clean: ['src/main/webapp/js/tmp'],

		sass: {
			dist: {
				options: {
					outputStyle: 'compressed',
					sourceMap: true
				},
				files: {
					'src/main/resources/net/netm/platform/client/tapestry/mediathek/css/<%= pkg.name %>.css': 'src/main/resources/net/netm/platform/client/tapestry/mediathek/sass/style.scss'
				}

			}
		},

		watch: {
			layout: {
				files: ['src/main/resources/net/netm/platform/client/tapestry/mediathek/sass/**/*.scss'],
				tasks: ['layout']
			},
			js: {
				files: ['src/main/webapp/js/scripts/**/*.js'],
				tasks: ['js']
			}
			// ,
			// beautify:{
			// 	files: ['src/main/java/**/*.java','src/main/resources/**/*.tml','src/main/resources/net/netm/platform/client/tapestry/mediathek/lib/js/thumbnail-slider.js'],
			// 	tasks: ['beautify-all']
			// }
		}
	});

	// JS tasks
	//grunt.registerTask('js', ['babel', 'requirejs', 'concat:dist', 'uglify', 'clean']);

	// CSS/SASS tasks
	//grunt.registerTask('layout', ['sass','jsbeautifier']);

	//tml
	grunt.registerTask('beauty', ['jsbeautifier']);

	// NPM Build task
	//grunt.registerTask('build', ['js', 'sass']);

	// Expose the default watch task
	grunt.registerTask('default', ['watch']);
};